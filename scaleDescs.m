function hists = scaleDescs(hists)
% normalize histograms, if there's more than one
if size(hists,1)~=1
    % init output variable
    hists_scaled = 0.*hists;
    % foreach bin, find min and max in the input histograms
    for i =1:size(hists,2)
        mi = min(hists(:,i));
        ma = max(hists(:,i));
        % scale bin for each descriptor so that over all descriptors the
        % bin uses its full allowed space of [-1;+1]
        if (ma ~= 0)
            hists_scaled(:,i) = 2*((hists(:,i) - mi)/(ma-mi)) - 1;
        else
            hists_scaled(:,i) = hists(:,i);
        end
    end
    hists = hists_scaled;
else
    hists=hists;
end
end