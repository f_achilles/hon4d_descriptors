% write the classification function
clc
close all;
desc_fold = '.';

featurePrefix = 'HON4DFeature';

trainingDesc = [];
testingDesc = [];
trainingLbls = {};
testingLbls = {};
trainingFileInds = [];
testingFileInds = [];

trainingDescWaving = [];
testingDescWaving = [];
trainingLblsWaving = [];
testingLblsWaving = [];

dDesc = dir([desc_fold filesep '*.txt']);
% trainActors = 1:(length(dDesc)-50);
% testActors = (length(dDesc)-49):length(dDesc);%[1:4]+24;

trainActors ={'AnnaMira','Christian','Claudia','Jan'}; %'Christian','Claudia',
trainActions = {'HandLeft','HandRight','HeadLeft','HeadRight','LegLeft','LegRight','So4Left','So4Right'};
trainIndices = [1 2 4];

testActors ={'AnnaMira','Christian','Claudia','Jan'};
testActions = {'HandLeft','HandRight','HeadLeft','HeadRight','LegLeft','LegRight','So4Left','So4Right'};
testIndices = [3];

for i=1:length(dDesc)
    % parse the descriptor filename
    dname = dDesc(i).name;
    d = load([desc_fold filesep dname]);
    
    [action,b,c]=regexp(dname,'_(\w+\d*\w+)_','tokens');
    action=action{1};
    
    [~,a]=regexp(dname,featurePrefix);
    actor=dname((a+1):(b-1));
    
    index=str2double(dname(c+1));
    
    % assign descriptors and labels
    % training
    if any(strcmp(trainActors,actor)) && any(strcmp(trainActions,action)) && any(trainIndices==index)
        trainingDesc = [trainingDesc;d];
        trainingLbls = [trainingLbls;action];
        trainingFileInds = [trainingFileInds;i];
    end
    % testing
    if any(strcmp(testActors,actor)) && any(strcmp(testActions,action)) && any(testIndices==index)
        testingDesc = [testingDesc;d];
        testingLbls = [testingLbls;action];
        testingFileInds = [testingFileInds;i];
    end

end
% upscaling the bins to full allowed region [-1/+1]
% my implementation:
allDesc = scaleDescs([trainingDesc;testingDesc]);
% trainingDesc = allDesc(1:size(trainingDesc,1),:);
% testingDesc = allDesc((size(trainingDesc,1)+1):end,:);

% their implementation:
trainingDesc = scaleDescs(trainingDesc);
testingDesc = scaleDescs(testingDesc);

% convert label names to index numbers
allLabels=[trainingLbls;testingLbls];
labelInds=zeros(size(allLabels));
labelList = unique(allLabels); %!
numOfLabels=numel(labelList);
for iL=1:numOfLabels
    appearance=strcmp(allLabels,labelList(iL));
    labelInds(appearance)=iL;
end
labelIndsTraining=labelInds(1:numel(trainingLbls));
labelIndsTesting=labelInds((numel(trainingLbls)+1):end);

svmParams = '-t 1 -g 0.125 -d 3';
model = libsvmtrain(labelIndsTraining,trainingDesc,svmParams);
predicted_labels = svmpredict(labelIndsTesting,testingDesc,model);
acc = 100*sum(predicted_labels == labelIndsTesting)/length(labelIndsTesting);

disp(['The achieved classification accuracy was: ' num2str(acc) '%'])

DIM=120;
