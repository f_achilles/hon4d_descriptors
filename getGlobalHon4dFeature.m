function HONV_feauture = getGlobalHon4dFeature(frame_count,dx_img,dy_img,dz_img,center_x, center_y, ...
    center_z, cell,P)
% Computes global HON4D feature without using Patches
% HONV_feauture = getGlobalHon4dFeature(frame_count,dx_img,dy_img,dz_img,center_x, center_y, ... center_z, cell,P)


% Note: this code is not optimized, and can be easily made faster

    DIM = cell.DIM;

    HONV_feauture = [];

    dx_img = dx_img(:);
    dy_img = dy_img(:);
    dz_img = dz_img(:);

    validInd = intersect(intersect(find(dx_img~=0),find(dy_img~=0)),find(dz_img~=0));

    dx_img = dx_img(validInd);
    dy_img = dy_img(validInd);
    dz_img = dz_img(validInd);



    gmags = sqrt(dx_img.^2 + dy_img.^2 + dz_img.^2);
    gmags_rep = repmat(gmags,1,DIM);

    gmat = [dx_img,dy_img,dz_img,-1*ones(length(dx_img),1)];




    res = (P*gmat')';

    res_norm = res./gmags_rep;

    res_norm(isinf(res_norm)) = 0;
    res_norm(isnan(res_norm)) = 0;


    res_norm = res_norm - 1.3090;
    res_norm(res_norm<0) = 0;


    vecmag = sqrt(sum(res_norm.^2,2));
    vecmag_rep = repmat(vecmag,1,DIM);


    res_norm = res_norm./vecmag_rep;
    res_norm(isinf(res_norm)) = 0;
    res_norm(isnan(res_norm)) = 0;


    res_norm = res_norm./length(validInd);

    HONV_feauture = [HONV_feauture,sum(res_norm,1)];
            
end