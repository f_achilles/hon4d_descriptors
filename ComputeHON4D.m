%% Read depth sequence
clc,close all;
filename_depth = 'a01_s01_e01_depth.bin';


% % Read the depth sequence
% [depth, skeleton_mask] =  ReadDepthBin(filename_depth);

% If the depth sequence is not stored in a bin file, you can comment the
% above line, and uncomment the lines below to read an avi file.

% filename_depth = 'S1_C1_U1_E1.oni_Depth.avi';
% avif = mmreader(filename_depth);
% depth = double(zeros(avif.Height,avif.Width,avif.NumberOfFrames));
% for i=1:avif.NumberOfFrames
%     frame = read(avif,i);
%     depth(:,:,i) = double(rgb2gray(frame));
%     disp(i);
% end
matFiles=dir('C:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\allLabelledParts\*.mat');
for iFile=1:numel(matFiles)
% using own loading routine for .mat stored depth sequences
[depth, depthSeqFile] = loadDepthSequence(['C:\Users\Felix\Projects\Kinect Grosshadern\matlabRecordings\allLabelledParts\' matFiles(iFile).name],'mat');
depth=double(depth);

% compute derivatives of depth
num_frames = size(depth,3);
gx = zeros(size(depth,1),size(depth,2),size(depth,3)-1);
gy = zeros(size(depth,1),size(depth,2),size(depth,3)-1);
gz = zeros(size(depth,1),size(depth,2),size(depth,3)-1);
for indFrame=1:num_frames-1
    im1 = medfilt2(depth(:,:,indFrame), [5 5]);
    im2 = medfilt2(depth(:,:,indFrame+1), [5 5]);
    [dx,dy,dz] = gradient(cat(3,im1,im2),1,1,1);
    
    gx(:,:,indFrame) = dx(:,:,1);
    gy(:,:,indFrame) = dy(:,:,1);
    gz(:,:,indFrame) = dz(:,:,1);
end


%% Compute HON4D 

% HON4D parameters
DIM = 120; % number of 4D projectors
P = loadProjector('000.txt',DIM);
cell = [];
cell.numR = 4;
cell.numC = 4;
cell.numD = 1;
cell.width = size(depth,2);
cell.height = size(depth,1);
cell.depth = size(depth,3);
cell.DIM = DIM;

% Find HON4D for an example point at coordinates [center_x,center_y] at depth frame number f
center_x = round(cell.width/2);
center_y = round(cell.height/2);
f=round(cell.depth/2);
center_z = -1; % not used
HON4Dfeauture = get_honv4_feature(f,gx,gy,gz,center_x, center_y, ...
    center_z, cell,P);

% prepare as comma separated output
CSVfeature = sprintf('%.8f,',HON4Dfeauture);
% crop last comma
CSVfeature = CSVfeature(1:(end-1));
featureFile=fopen(['HON4DFeature' depthSeqFile(1:(end-4)) '.txt'],'a');
fwrite(featureFile,CSVfeature);
fclose(featureFile);

end



