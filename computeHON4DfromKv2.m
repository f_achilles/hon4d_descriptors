%% Read depth sequence
dataRoot = 'I:\simulatedframes';
featureTarget = 'C:\Projects\miccai2014depthstreampaper\code\features';
matFiles=dir([dataRoot filesep '*.mat']);

% how many frames should make up the temporal window?
dT = 30;

for iFile=2:numel(matFiles)
load(fullfile(dataRoot,matFiles(iFile).name));
[~,cSeqName]=fileparts(matFiles(iFile).name);
for iAct = 1 %:numel(actContainer)
% compute spatiotemporal gradients
    cSeq = single(actContainer{iAct});
    num_frames = size(cSeq,3);
    gx = zeros(size(cSeq)-[0 0 1],'single');
    gy = zeros(size(gx),'single');
    gz = zeros(size(gx),'single');
    for iFrame=1:num_frames-1
        im1 = medfilt2(cSeq(:,:,iFrame), [5 5]);
        im2 = medfilt2(cSeq(:,:,iFrame+1), [5 5]);
        [dx,dy,dz] = gradient(cat(3,im1,im2),1,1,1);

        gx(:,:,iFrame) = dx(:,:,1);
        gy(:,:,iFrame) = dy(:,:,1);
        gz(:,:,iFrame) = dz(:,:,1);
    end

    % Compute HON4D
    % HON4D parameters
    DIM = 120; % number of 4D projectors
    P = loadProjector('000.txt',DIM);
    cell = [];
    cell.numR = 4;
    cell.numC = 4;
    cell.numD = 1;
    cell.width = size(cSeq,2);
    cell.height = size(cSeq,1);
    cell.depth = dT;
    cell.DIM = DIM;

    % Find HON4D for an example point at coordinates [center_x,center_y] at depth frame number f
    center_x = round(cell.width/2);
    center_y = round(cell.height/2);
    for iTimeStep = 1:floor(num_frames/dT)
        f=round(cell.depth/2)+dT*(iTimeStep-1);
        center_z = -1; % not used
        HON4Dfeauture = get_honv4_feature(f,gx,gy,gz,center_x,center_y, ...
            center_z, cell,P); %length: 1920
        % prepare as comma separated output
        CSVfeature = sprintf('%.8f,',HON4Dfeauture);
        % crop last comma
        CSVfeature = CSVfeature(1:(end-1));
        cFileName = sprintf('%s%sHON4DFeature_%s_act%d_timestep%d.txt',featureTarget,filesep,cSeqName,iAct,iTimeStep);
        featureFile=fopen(cFileName,'a');
        fwrite(featureFile,CSVfeature);
        fclose(featureFile);
        disp(['''' cFileName ''' was successfully written.'])
    end
end
end



